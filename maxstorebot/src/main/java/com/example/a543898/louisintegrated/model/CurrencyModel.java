package com.example.a543898.louisintegrated.model;

/**
 * Created by 540472 on 3/21/2017.
 */
public class CurrencyModel {
    Double currencyvalue;

    public Double getCurrencyvalue() {
        return currencyvalue;
    }

    public void setCurrencyvalue(Double currencyvalue) {
        this.currencyvalue = currencyvalue;
    }
}
