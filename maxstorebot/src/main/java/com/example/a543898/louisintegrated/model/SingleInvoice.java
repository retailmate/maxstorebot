package com.example.a543898.louisintegrated.model;

/**
 * Created by 599584 on 3/21/2017.
 */

public class SingleInvoice {
    String heading;

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }
}
