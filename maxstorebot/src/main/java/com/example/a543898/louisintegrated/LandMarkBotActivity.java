
package com.example.a543898.louisintegrated;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class LandMarkBotActivity extends Activity  {
    Spinner spinner;
    ArrayAdapter<CharSequence> adapter;
    TextView language;
    Button confirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final String languageToLoad  = "en"; // change your language here
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        setContentView(R.layout.frontscreen);
        spinner=(Spinner)findViewById(R.id.spinner);
        language=(TextView) findViewById(R.id.selected_lang);
        confirm=(Button) findViewById(R.id.confirm);
        adapter= ArrayAdapter.createFromResource(this,R.array.languages,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);


         spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {



                if(position==0){
                    language.setText("English");
                    confirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(LandMarkBotActivity.this, MainActivity.class);
                            intent.putExtra("languageToLoad","en");
                            startActivity(intent);
                        }
                    });

                }
                else if(position==1){
                    language.setText("عربى");
                    confirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(LandMarkBotActivity.this, MainActivity.class);
                            intent.putExtra("languageToLoad","ar");
                            startActivity(intent);

                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
    }
}

